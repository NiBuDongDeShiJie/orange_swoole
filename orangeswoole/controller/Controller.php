<?php
namespace orangeswoole\controller;

/**
 * 基础控制器
 * Class Controller
 * @package orangeswoole
 */
class Controller
{
    /**
     * swoole server类
     * @var null
     */
    public $server = null;

    /**
     * 客户端连接id
     * 每个客户端唯一
     * @var int
     */
    public $fd = 0;

    /**
     * Controller constructor.
     * @param null $server
     * @param int $fd
     */
    public function __construct($server = null, $fd = 0)
    {
        $this->server = $server;
        $this->fd = $fd;
    }
}
