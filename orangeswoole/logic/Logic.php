<?php
namespace orangeswoole\logic;

/**
 * Class Logic
 * @package orangeswoole
 */
class Logic
{
    /**
     * swoole server类
     * @var null
     */
    public $server = null;

    /**
     * 客户端连接id
     * 每个客户端唯一
     * @var int
     */
    public $fd = 0;

    /**
     * Logic constructor.
     * @param null $server
     * @param int $fd
     */
    public function __construct($server = null, $fd = 0)
    {
        $this->server = $server;
        $this->fd = $fd;
    }
}
