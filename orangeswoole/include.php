<?php
/**
 * 公共包含文件
 */
include_once 'define.php';
include_once 'helper.php';
include_once APPLICATION_PATH.'/functions.php';
include_once CONFIG_PATH.'/define.php';

/**
 * 开启协程对curl，mysql，redis等的支持，真正的hook所有类型，包括CURL
 */
\Swoole\Runtime::enableCoroutine($flags = SWOOLE_HOOK_ALL);