<?php
namespace orangeswoole\event;

/**
 * 默认事件类-子类可以重写里面的方法达到想要的效果
 * Class Event
 * @package orangeswoole
 */
class EventBase
{
    /**
     * swoole服务类
     * @var null
     */
    public $server = null;

    /**
     * swoole给客户端分配的唯一id
     * @var null
     */
    public $fd = null;

    /**
     * 初始化的时候传入swoole server类
     * EventBase constructor.
     * @param $server
     */
    public function __construct($server)
    {
        $this->server = $server;
    }

    /**
     * 监听连接进入事件-暂时没有到的回调事件
     * @author zk
     * @param $server
     * @param $fd
     */
    public function onConnect($server, $fd)
    {
        // todo 做一些初始化工作
    }

    /**
     * 进程启动事件，可以放一些定时任务等等
     * @author zk
     * @param $server
     * @param $workerId
     */
    public function onWorkerStart($server, $workerId)
    {
        // todo 其他的需要在进程开始事件中执行的任务
    }

    /**
     * 清除所有定时器和事件监听
     * @author zk
     * @param $server
     * @param $workerId
     */
    public function onWorkerExit($server, $workerId)
    {
        // todo 做一些收尾清理工作
    }

    /**
     * 客户端和服务器端连接关闭事件 清理内存占用
     * @author zk
     * @param $server
     * @param $fd
     * @param $reactorId
     */
    public function onClose($server, $fd, $reactorId)
    {
        // todo 做一些收尾清理工作
    }

    /**
     * 进程停止事件
     * @author zk
     * @param $server
     * @param $id
     */
    public function onWorkerStop($server, $id)
    {
        // todo 做一些收尾清理工作
    }

    /**
     * websocket专用连接事件
     * 客户端请求连接到服务器端
     * @author zk
     * @param $server
     * @param $request
     */
    public function onOpen($server, $request)
    {
        // todo 当连接到服务器的时候，处理登录之类的业务
    }

    /**
     * websocket专用消息事件
     * 客户端向服务器端发送消息
     * @author zk
     * @param $server
     * @param $frame
     */
    public function onMessage($server, $frame)
    {
        // todo 当客户端发送消息过来的时候处理对应的业务
    }
}
