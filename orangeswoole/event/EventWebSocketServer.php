<?php
namespace orangeswoole\event;

/**
 * 默认事件类-子类可以重写里面的方法达到想要的效果
 * Class EventWebSocketServer
 * @package orangeswoole
 */
class EventWebSocketServer extends EventBase
{
    /**
     * 客户端向服务器发送消息事件
     * 由onMessage事件控制路由转发
     * @author zk
     * @param $server
     * @param $frame
     * @return bool|void
     */
    public function onMessage($server, $frame)
    {
        // 赋值连接id
        $fd = $frame->fd;
        // 心跳检测
        if ($frame->data == 'ping') {
            return send($this->server, $fd, 'pong');
        }
        $requestData = swoole_substr_json_decode($frame->data, 0, -1, true);
        if (empty($requestData) || !isset($requestData['route']) || !isset($requestData['params'])) {
            return send($this->server, $fd, returnMessage(1101, '参数格式错误'));
        }
        $routeArray = explode('/', $requestData['route']);
        if (count($routeArray) != 3) {
            return send($this->server, $fd, returnMessage(1102, '访问路由错误，你应该像这样：模块/控制器/方法'));
        }
        list($module, $controller, $action) = $routeArray;
        $params = $requestData['params'];
        try {
            $controllerPath = '\\app\\' . $module . '\\controller\\' . $controller . 'Controller';
            $controllerObject = new $controllerPath($this->server, $fd);
            $startTime = microtime(true);
            $result = $controllerObject->$action($params);
            $endTime = microtime(true);
            $result['run_time'] = number_format(($endTime - $startTime), 10) . 's';
            return send($this->server, $fd, $result);
        } catch (\Throwable $exception) {
            if (isset(CONFIG['debug']) && CONFIG['debug']) {
                $message = 'server-error message:'.$exception->getMessage().' line:'.$exception->getLine().' file:'.$exception->getFile();
            } else {
                $message = 'server-error';
            }
            return send($this->server, $fd, returnMessage(1099, $message));
        }
    }

    /**
     * onWorkerStart
     * @author zk
     * @param $server swoole服务对象
     * @param $workerId 进程id
     * @throws \Exception
     */
    public function onWorkerStart($server, $workerId)
    {

    }
}
