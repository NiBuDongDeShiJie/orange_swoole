<?php
namespace orangeswoole\event;

/**
 * 默认事件类-子类可以重写里面的方法达到想要的效果
 * Class EventRedisServer
 * @package orangeswoole
 */
class EventRedisServer extends EventBase
{
    /**
     * onWorkerStart
     * @author zk
     * @param $server swoole服务对象
     * @param $workerId 进程id
     * @throws \Exception
     */
    public function onWorkerStart($server, $workerId)
    {

    }

    /**
     * 通过onHGet方法来实现路由控制
     * 实现redis的HGet方法
     * 也可以封装redis的客户端方法，比如get，set，hget等按照自己想要的操作来封装
     * 这里推荐使用redis->hget
     * @param $server swoole的server对象
     * @param $fd
     * @param $data
     * @return mixed
     */
    public function onHGet($server, $fd, $data)
    {
        // 这2个参数必须放前面
        $this->fd = $fd;
        $this->server = $server;
        // 路由访问开始
        $routeInfo = reset($data);
        $params = end($data);
        $routeArray = explode('/', $routeInfo);
        if (count($routeArray) != 3) {
            return $this->send(returnData(1001, '访问路由错误，你应该像这样：模块/控制器/方法'));
        }
        list($module, $controller, $action) = $routeArray;
        try {
            $controllerPath = '\\app\\' . $module . '\\controller\\' . $controller . 'Controller';
            $controllerObject = new $controllerPath();
            if (!is_array($params)) {
                $params = swoole_substr_json_decode($params, 0, -1, true);
            }
            $startTime = microtime(true);
            $result = $controllerObject->$action($params);
            $endTime = microtime(true);
            $result['run_time'] = number_format(($endTime - $startTime), 10) . 's';
            return $this->send($result);
        } catch (\Throwable $exception) {
            if (isset(CONFIG['debug']) && CONFIG['debug']) {
                $message = 'server-error message:'.$exception->getMessage().' line:'.$exception->getLine().' file:'.$exception->getFile();
            } else {
                $message = 'server-error';
            }
            return $this->send(returnData(1099, $message));
        }
    }

    /**
     * 发送数据到客户端
     * @author zk
     * @param $data
     * @return mixed
     */
    private function send($data)
    {
        //最终以json格式返回给redis客户端
        $data = json_encode($data);
        $data = \Swoole\Redis\Server::format(\Swoole\Redis\Server::STRING,  $data);
        return $this->server->send($this->fd, $data);
    }
}
