<?php

/**
 * 获取config目录下的配置文件信息
 * @param $file
 * @param $key
 * @return mixed
 */
function config($file, $key = null)
{
    $config = include CONFIG_PATH.'/'.$file.'.php';
    if (is_null($key)) {
        return $config;
    }
    return $config[$key];
}

/**
 * 获取模块下面的配置文件
 * @param $file
 * @param $key
 * @return mixed
 */
function moduleConfig($file, $key = null)
{
    $config = include APPLICATION_PATH.'/'.MODULE.'/'.$file.'.php';
    if (is_null($key)) {
        return $config;
    }
    return $config[$key];
}