<?php
namespace orangeswoole\server;

/**
 * 启动，停止，重启相关命令
 * Class commands
 */
class Commands
{
    //主进程pid
    public $pid = null;

    /**
     * commands constructor.
     */
    public function __construct()
    {
        //如果pid存在赋值pid
        if (file_exists(CONFIG['swoole']['pid_file'])) {
            $pid = file_get_contents(CONFIG['swoole']['pid_file']);
            if (empty($pid)) {
                exit("pid not exist \n");
            }
            $this->pid = $pid;
        }
    }

    /**
     * 启动swoole服务
     * @param $swooleServer swoole服务类
     * @param $config 当前swoole的配置
     * @return mixed
     */
    public function start($swooleServer, $config)
    {
        return new $swooleServer($config);
    }

    /**
     * 停止swoole服务
     * @return string
     */
    public function stop()
    {
        if (is_null($this->pid)) {
            return "pid not exist or server is stopped \n";
        }
        //判断进程是否存在
        if (!\swoole_process::kill($this->pid, 0)) {
            return "pid :{$this->pid} not exist \n";
        }
        \swoole_process::kill($this->pid);
        //等待5秒
        $time = time();
        $msg = '';
        while (true) {
            usleep(1000);
            if (!\swoole_process::kill($this->pid, 0)) {
                $msg = "server stop success \n";
                break;
            } else {
                if (time() - $time > 5) {
                    $msg = "stop server fail.try again \n";
                    break;
                }
            }
        }
        return $msg;
    }

    /**
     * 重启
     * @return string
     */
    public function restart()
    {
        if (is_null($this->pid)) {
            return "pid not exist or server is stopped \n";
        }
        $sig = SIGUSR1;
        //检测当前pid是否存在
        if (!\swoole_process::kill($this->pid, 0)) {
            return "pid :{$this->pid} not exist \n";
        }
        \swoole_process::kill($this->pid, $sig);
        return "send server restart success command \n";
    }

    /**
     * 服务状态检测
     * @return string
     */
    public function status()
    {
        if (is_null($this->pid)) {
            return "pid not exist or server is stopped \n";
        }
        //检测当前pid是否存在
        if (!\swoole_process::kill($this->pid, 0)) {
            return "server is stopped \n";
        }
        return "server is running \n";
    }
}