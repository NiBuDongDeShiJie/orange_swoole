<?php
namespace orangeswoole\server;

use Swoole\WebSocket\Server;

/**
 * 基于swoole的websocket Server服务类
 * 启动服务入口基类
 * Class SwooleRedisServer
 * @package orangeswoole
 */
class SwooleWebSocketServer extends ServerBase
{
    /**
     * websocket 服务初始化
     * SwooleWebSocketServer constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        if ($config['is_ssl']) {
            $this->server = new Server($config['swoole']['listen'], $config['swoole']['port'], $config['swoole_mode'], SWOOLE_SOCK_TCP | SWOOLE_SSL);
        } else {
            $this->server = new Server($config['swoole']['listen'], $config['swoole']['port'], $config['swoole_mode']);
        }
        // 实例化各个模块下面的事件处理类
        $this->event = new $config['swoole_event']($this->server);
        // 配置swoole options 传入options参数之前，先去掉listen，port参数，这2个参数非swoole option的参数，不然有警告
        unset($config['swoole']['listen']);
        unset($config['swoole']['port']);
        $this->server->set($config['swoole']);

        //连接事件
        $this->server->on('open', function ($server, $request) {
            $this->event->onOpen($server, $request);
        });

        //消息事件
        $this->server->on('message', function ($server, $frame) {
            $this->event->onMessage($server, $frame);
        });

        // 监听连接进入事件-暂时没有到的回调事件-配置了dispatch_mode=1的时候，该事件不要启用
        if (!isset($config['swoole']['dispatch_mode']) || !in_array($config['swoole']['dispatch_mode'], [1])) {
            $this->server->on('Connect', function ($server, $fd) {
                // todo
            });
        }

        // 客户端和服务器端连接关闭事件，清理内存占用-配置了dispatch_mode=1的时候，该事件不要启用
        if (!isset($config['swoole']['dispatch_mode']) || !in_array($config['swoole']['dispatch_mode'], [1])) {
            $this->server->on('Close', function ($server, $fd, $reactorId){
                // todo
            });
        }

        // 进程启动事件
        $this->server->on('WorkerStart', function ($server, $id) {
            $this->event->onWorkerStart($server, $id);
        });

        // 进程结束事件
        $this->server->on('WorkerExit', function ($server, $id) {
            $this->event->onWorkerExit($server, $id);
        });

        // 进程停止事件-暂时没有到的回调事件
        $this->server->on('WorkerStop', function ($server, $id) {
            // todo
        });

        // 进程结束事件-暂时没有到的回调事件
        $this->server->on('WorkerExit', function ($server, $id) {
            // todo
        });

        //启动
        $this->server->start();
    }
}
