<?php
namespace orangeswoole\server;

/**
 * 运行管理.
 * Class Run
 * @package orangeswoole
 */
class Run
{
    /**
     * 命令行输入参数
     * @var
     */
    public $input;

    /**
     * 服务的文件名称
     * @var
     */
    public $serverFileName;

    /**
     * Run constructor.
     * @param $argv 命令行输入参数
     * @param $serverFileName 服务的文件名称
     */
    public function __construct($argv, $serverFileName)
    {
        $this->input = $argv;
        $this->serverFileName = $serverFileName;
    }

    /**
     * 集成命令方式启动停止重启
     */
    public function run()
    {
        $command = isset($this->input[1]) ? $this->input[1] : '';
        $daemonize = isset($this->input[2]) ? $this->input[2] : '';
        $pathInfo = pathinfo($this->serverFileName);
        $module = substr($pathInfo['filename'], 0, strlen($pathInfo['filename']) - strlen('SwooleServer'));
        $module = strtolower($module);
        // 模块名称
        define('MODULE', $module);
        // 模块路径
        define('MODULE_PATH', APPLICATION_PATH.'/'.$module);
        // 当前模块的配置文件
        $config = include_once MODULE_PATH.'/config.php';
        // 如果当前模块设置了functions.php函数文件则加载它
        if (is_file(MODULE_PATH . '/functions.php')) {
            include_once MODULE_PATH.'/functions.php';
        }
        // 如果输入-d参数则以守护模式启动
        if ($daemonize == '-d') {
            $config['swoole']['daemonize'] = 1;
        }
        // 当前模块的配置文件
        define('CONFIG', $config);
        // 命令判断
        if (!in_array($command, ['start', 'stop', 'restart', 'status'])) {
            exit("请输入：start|stop|restart|status\n");
        }
        $commands = new Commands();
        switch ($command) {
            // 启动
            case 'start':
                $swooleServer = '\swooleserver'.'\\'.$pathInfo['filename'];
                $commands->start($swooleServer, CONFIG);
            // 停止
            case 'stop':
                exit($commands->stop());
            // 重启
            case 'restart':
                exit($commands->restart());
            // 状态
            case 'status':
                exit($commands->status());
            default:
                exit('命令错误');
        }
    }
}