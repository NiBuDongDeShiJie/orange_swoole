<?php
namespace orangeswoole\server;

use Swoole\Redis\Server;

/**
 * 基于swoole的redisServer服务类，用来处理有协程异步IO需求的业务
 * 启动服务入口基类
 * Class SwooleRedisServer
 * @package orangeswoole
 */
class SwooleRedisServer extends ServerBase
{
    /**
     * socket服务初始化
     * RedisSwooleServer constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        if ($config['is_ssl']) {
            $this->server = new Server($config['swoole']['listen'], $config['swoole']['port'], $config['swoole_mode'], SWOOLE_SOCK_TCP | SWOOLE_SSL);
        } else {
            $this->server = new Server($config['swoole']['listen'], $config['swoole']['port'], $config['swoole_mode']);
        }
        // 实例化各个模块下面的事件处理类
        $this->event = new $config['swoole_event']($this->server);
        // 配置swoole options 传入options参数之前，先去掉listen，port参数，这2个参数非swoole option的参数，不然有警告
        unset($config['swoole']['listen']);
        unset($config['swoole']['port']);
        $this->server->set($config['swoole']);

        // 监听连接进入事件-暂时没有用到的回调事件-配置了dispatch_mode=1的时候，该事件不要启用
        if (!isset($config['swoole']['dispatch_mode']) || !in_array($config['swoole']['dispatch_mode'], [1])) {
            $this->server->on('Connect', function ($server, $fd) {
                // todo
            });
        }

        // redis操作命令HGET 所有的协程指令都是从客户端redis的HGET方法发出，这里就只处理redis的HGET命令，这里也可以封装redis的其他命令
        // fd swoole分配给客户端的id、$data 是客户端传入的参数、$this->server swoole服务类
        $this->server->setHandler('HGET', function ($fd, $data) {
            $this->event->onHGet($this->server, $fd, $data);
        });

        // 客户端和服务器端连接关闭事件，清理内存占用-配置了dispatch_mode=1的时候，该事件不要启用
        if (!isset($config['swoole']['dispatch_mode']) || !in_array($config['swoole']['dispatch_mode'], [1])) {
            $this->server->on('Close', function ($server, $fd, $reactorId){
                // todo
            });
        }

        // 进程启动事件
        $this->server->on('WorkerStart', function ($server, $id) {
            $this->event->onWorkerStart($server, $id);
        });

        // 进程结束事件
        $this->server->on('WorkerExit', function ($server, $id) {
            $this->event->onWorkerExit($server, $id);
        });

        // 进程停止事件-暂时没有用到的回调事件
        $this->server->on('WorkerStop', function ($server, $id) {
            // todo
        });

        // 进程结束事件-暂时没有用到的回调事件
        $this->server->on('WorkerExit', function ($server, $id) {
            // todo
        });

        //启动
        $this->server->start();
    }
}

