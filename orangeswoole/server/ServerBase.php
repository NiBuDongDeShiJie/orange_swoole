<?php
namespace orangeswoole\server;

include_once dirname(dirname(__FILE__)) . '/include.php';
// 根据配置信息设置php的报错级别
$errorReporting = config('app', 'error_reporting');
error_reporting($errorReporting);

/**
 * 基础服务
 * Class ServerBase
 * @package orangeswoole\server
 */
class ServerBase
{
    /**
     * 服务实例
     * @var Server
     */
    public $server;

    /**
     * 事件类
     * @var
     */
    public $event;
}