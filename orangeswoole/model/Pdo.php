<?php
namespace orangeswoole\model;

/**
 * pdo数据库操作类
 * Class db
 * @package orangeswoole\model
 */
class Pdo {

    /**
     * @var \PDO|null
     */
    public $pdo = null;

    /**
     * 表名称在model中可以覆盖
     * @var mixed|null
     */
    public $table = null;

    /**
     * 数据库表前缀
     * @var mixed|string
     */
    public $prefix = '';

    /**
     * 默认db连接配置key,在model中可以覆盖
     * @var string
     */
    public $connect = 'default';

    /**
     * db constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        // 数据库配置选择，优先加载模块下面的数据库配置信息
        $moduleDatabasePath = MODULE_PATH . '/database.php';
        if (is_file($moduleDatabasePath)) {
            $mysqlConfig = include $moduleDatabasePath;
        } else {
            $mysqlConfig = include CONFIG_PATH . '/database.php';
        }
        if (!$mysqlConfig) {
            throw new \Exception('mysql config not fond');
        }
        // 配置文件选择
        $config = $mysqlConfig[$this->connect];
        // 表处理
        $childrenClassName = get_class($this);
        $childrenClassName = explode('\\', $childrenClassName);
        list(, , , $tableClassName) = $childrenClassName;
        if (is_null($this->table)) {
            $tableClassName = explode('Model', $tableClassName);
            $tableName = reset($tableClassName);
            $this->table = $tableName;
        }
        $this->prefix = $config['prefix'];
        $dsn = "mysql:host={$config['host']};port={$config['port']};dbname={$config['database']};charset={$config['charset']}";
        $this->pdo = new \PDO($dsn, $config['user'], $config['password']);
    }

    /**
     * @author zk
     * @param $sql
     * @return array
     */
    public function query($sql)
    {
        $query = $this->pdo->query($sql);
        $result = $query->fetchAll(\PDO::FETCH_ASSOC);
        $this->pdo = null;
        return $result;
    }
}