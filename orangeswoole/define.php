<?php
//项目目录
define('PROJECT_PATH', dirname(__DIR__));
//swoole服务目录
define('SWOOLE_SERVER_PATH', PROJECT_PATH.'/swooleserver');
//composer扩展目录
define('VENDOR_PATH', PROJECT_PATH.'/vendor');
//应用目录
define('APPLICATION_PATH', PROJECT_PATH.'/application');
//运行日志，缓存等目录
define('RUNTIME_PATH', PROJECT_PATH.'/runtime');
//框架目录
define('ORANGE_SWOOLE_PATH', PROJECT_PATH.'/orangeswoole');
//config目录
define('CONFIG_PATH', PROJECT_PATH.'/config');
//tools目录
define('TOOLS_PATH', PROJECT_PATH.'/tools');