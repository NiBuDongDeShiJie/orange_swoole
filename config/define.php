<?php
/**
 * 项目定义常量
 * 这个文件不能删除
 * 在底层是自动加载了这个文件的
 */

/**
 * 正服和测服之间的判断
 * swoole_get_local_ip()获取网卡信息
 */
$isProduce = false;

$serverIpArray = \swoole_get_local_ip();
// 默认取eth0网卡的信息
$serverIp = isset($serverIpArray['eth0']) ? $serverIpArray['eth0'] : reset($serverIpArray);
// 正服IP数组 注意这个正服负载的机器可能会增加到时候把其他机器的本地IP加进来
$produceIpArray = [
    '0.0.0.1', // 正服的本机IP 负载机器1
    '0.0.0.2', // 正服的本机IP 负载机器2
    '0.0.0.3', // 正服的本机IP 负载机器3
];
if (in_array($serverIp, $produceIpArray)) {
    $isProduce = true;
}

// 当前服务器的本机IP地址
define('SERVER_IP', $serverIp);

// 是否是正服
define('IS_PRODUCE', $isProduce);

// 其他定义在后面加