<?php

/**
 * 公共配置文件
 * 调用方法 config('common', 'key')
 */
return [
    // E_ALL & ~E_NOTICE 报告 E_NOTICE 之外的所有错误
    'error_reporting' => E_ALL
];