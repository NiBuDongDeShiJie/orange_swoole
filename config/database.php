<?php

/**
 * mysql数据库配置
 * default是默认的数据库配置信息,可以配置其他的数据库配置信息
 * 优先会加载模块下面的database.php文件
 */
return [
    'default' => [
        'host'      => 'host1', // 服务器地址
        'port'      => 3306, // 端口
        'user'      => 'user1', // 用户名
        'password'  => 'password1', // 密码
        'charset'   => 'utf8', // 编码
        'database'  => 'db', // 数据库名
        'prefix'    => 'xxx_' // 表前缀
    ],
    'other_db_config' => [
        'host'      => 'host2', // 服务器地址
        'port'      => 3306, // 端口
        'user'      => 'user2', // 用户名
        'password'  => 'password2', // 密码
        'charset'   => 'utf8', // 编码
        'database'  => 'db', // 数据库名
        'prefix'    => 'xxx_' // 表前缀
    ],
];