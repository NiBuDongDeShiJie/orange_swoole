<?php

/**
 * es 配置信息
 */

if (IS_PRODUCE) {
    return [
        // 正服-es客户端配置信息
        'elastic_search' => [
            'protocol' => 'http',
            'host' => '0.0.0.0',
            'port' => 0000,
            'user' => 'user',
            'password' => 'password'
        ]
    ];
} else {
    return [
        // 测服-es客户端配置信息
        'elastic_search' => [
            'protocol' => 'http',
            'host' => '0.0.0.0',
            'port' => 0000,
            'user' => '',
            'password' => ''
        ]
    ];
}