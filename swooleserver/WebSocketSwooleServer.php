<?php
namespace swooleserver;

include_once dirname(__DIR__).'/vendor/autoload.php';

use orangeswoole\server\Run;
use orangeswoole\server\SwooleWebSocketServer;

/**
 * 启动方式：php WebSocketSwooleServer.php start|start -d|stop|restart
 * WebSocketSwooleServer注意这里的命名 WebSocket要和模块的名称一致
 * 基于swoole的websocket服务类
 * WebSocketSwooleServer入口服务类
 * Class WebSocketSwooleServer
 * @package swooleserver
 */
class WebSocketSwooleServer extends SwooleWebSocketServer
{
    /**
     * 如果觉得父类的不满足需求，可以再这里重写父类的 __construct($config) {}所有内容
     * 这里要实现所有的父类的内容不能用parent::__construct($config);进行追加
     */
}
// 运行启动相关控制
$run = new Run($argv, $_SERVER['PHP_SELF']);
$run->run();
