<?php
namespace swooleserver;

include_once dirname(__DIR__).'/vendor/autoload.php';

use orangeswoole\server\Run;
use orangeswoole\server\SwooleRedisServer;

/**
 * 启动方式：php DemoSwooleServer.php start|start -d|stop|restart
 * 基于redis的RESP（Redis的序列化协议）协议，此处就是通过swoole模拟了一个redis服务器端，客户端通过php的redis客户端进行链接
 * DemoSwooleServer注意这里的命名Demo要和模块的名称一致（可以忽略大小写，底层会转换成小写处理）
 * 基于swoole的redisServer服务类，用来处理有协程异步IO需求的业务以及作为一个结合MQ的异步日志服务端
 * DemoSwooleServer.php入口服务类 你可以理解为 tp的index.php入口文件
 * Class DemoSwooleServer
 * @package swooleserver
 */
class DemoSwooleServer extends SwooleRedisServer
{
    /**
     * 如果觉得父类的不满足需求，可以再这里重写父类的 __construct($config) {}所有内容
     * 这里要实现所有的父类的内容不能用parent::__construct($config);进行追加
     */
}
// 通过cli命令行参数控制server的启动、关闭、重启
$run = new Run($argv, $_SERVER['PHP_SELF']);
$run->run();
