<?php
namespace app\common\validate;

use orangeswoole\Validate;

/**
 * tp5验证器，写法一致
 * Class TestValidate
 * @package app\common\validate
 */
class TestValidate extends Validate
{
    /**
     * 验证规则
     * @var array
     */
    protected $rule = [

    ];

    /**
     * 验证提示
     * @var array
     */
    protected $message = [

    ];

    /**
     * 使用场景
     * @var array
     */
    protected $scene = [

    ];
}