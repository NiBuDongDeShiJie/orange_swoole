<?php
namespace app\common\model;

/**
 * 用户表
 * Class UserModel
 * @package app\common\model
 */
class UserModel extends CommonModel
{

    /**
     * 和tp5一致，这里是定义当前模型所使用的数据库连接
     */
    public $connect = 'other_db_config';

    /**
     * 和tp5一致，这里是定义当前模型的表
     */
    // public $table = 'app_access_token';
}