<?php

/**
 * 使用频率高的函数放这里
 * 公共函数库
 * 框架已经自动加载了这个文件
 */

/**
 * 数据返回
 * @author zk
 * @param $code
 * @param $msg
 * @param array $data
 * @return mixed
 */
function returnData($code, $msg, $data = [])
{
    if ($data) {
        ksort($data);
    }
    $returnData['code'] = intval($code);
    $returnData['msg'] = $msg;
    $returnData['data'] = (object)$data;
    return $returnData;
}