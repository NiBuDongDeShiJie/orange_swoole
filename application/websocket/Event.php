<?php
namespace app\websocket;

use app\websocket\controller\LoginController;
use \orangeswoole\event\EventWebSocketServer;

/**
 * 事件类
 * 可以在这里单独写当前模块的事件
 * 并在事件里面处理自己的业务
 * Class Event
 * @package app\websocket
 */
class Event extends EventWebSocketServer
{
    /**
     * 进程启动初始化工作
     * 启动定时任务、初始化连接池之类的业务
     * onWorkerStart
     * @author zk
     * @param $server swoole服务对象
     * @param $workerId 进程id
     * @throws \Exception
     */
    public function onWorkerStart($server, $workerId)
    {
        // todo 进程启动初始化工作、启动定时任务、初始化连接池之类的业务
    }

    /**
     * 客户端连接到服务器事件
     * 在连接的时候处理登录相关业务
     * @author zk
     * @param $server swoole server 对象
     * @param $request 用户请求对象
     * @return bool|void
     */
    public function onOpen($server, $request)
    {
        $loginController = new LoginController();
        $startTime = microtime(true);
        $result = $loginController->login($request);
        $endTime = microtime(true);
        $result['run_time'] = ($endTime - $startTime) . 's';
        return send($this->server, $request->fd, $result);
    }
}
