<?php
namespace app\websocket\controller;

use app\common\controller\CommonController;
use app\websocket\logic\LoginLogic;

/**
 * 登录业务
 * Class LoginController
 * @package app\websocket\controller
 */
class LoginController extends CommonController
{
    /**
     * 登录业务
     * @author zk
     * @param $request
     * @return mixed
     */
    public function login($request)
    {
        $logic = new LoginLogic();
        return $logic->login($request);
    }
}