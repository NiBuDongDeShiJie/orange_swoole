<?php
namespace app\websocket\controller;

use app\common\controller\CommonController;
use app\websocket\logic\MessageLogic;

/**
 *
 * Class IndexController
 * @package app\websocket\controller
 */
class MessageController extends CommonController
{
    /**
     * 发送消息
     * @author zk
     * @param $data
     * @return mixed
     */
    public function message($data)
    {
        $logic = new MessageLogic($this->server, $this->fd);
        return $logic->message($data);
    }
}