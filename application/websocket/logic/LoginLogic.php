<?php
namespace app\websocket\logic;

use orangeswoole\logic\Logic;

class LoginLogic extends Logic
{
    /**
     * 登录
     * @author zk
     * @param $request
     * @return mixed
     */
    public function login($request)
    {
        $token = $request->get['token']; // 参数过滤等
        if (empty($token)) {
            return returnMessage(1001, '参数错误');
        }
        // 登陆成功之后通过redis绑定fd到token
        //$prefix = CONFIG['redis']['prefix'];
        // $connectionKey = $prefix.'connection_'.$token;
        //$this->redis->set($connectionKey, $request->fd, 3600);
        $returnData = [
            'user_info' => ['name' => '张三', 'user_id' => 1]
        ];
        return returnMessage(1000, '登录连接成功', 'login', $returnData);
    }
}