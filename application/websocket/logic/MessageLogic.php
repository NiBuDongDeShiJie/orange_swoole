<?php
namespace app\websocket\logic;

use orangeswoole\logic\Logic;

class MessageLogic extends Logic
{
    /**
     * 发送消息
     * @author zk
     * @param $data
     * @return mixed
     */
    public function message($data)
    {
        $sendData = [
            'message' => $data['message']
        ];
        send($this->server, $data['to'], returnMessage(1000, '接收到信息', 'receive_message', $sendData));
        return returnMessage(1000, '操作成功', 'send_message');
    }
}