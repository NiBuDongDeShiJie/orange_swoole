<?php
/**
 * 应用设置
 * 使用说明
 * cd 到 swooleserver目录
 * 命令行 php WebSocketSwooleServer.php start|restart|stop|status 其中 start -d 以守护进程模式启动
 */
return [
    // swoole官方配置，如需其他官方配置请自行修改
    'swoole' => [
        // 监听所有ip
        'listen' => '0.0.0.0',
        // swoole启动端口
        'port' => '3333',
        // 加入此参数后，执行php server.php将转入后台作为守护进程运行 1守护 0非守护
        'daemonize' => 0,
        // 通过此参数来调节主进程内事件处理线程的数量，以充分利用多核。默认会启用 CPU 核数相同的数量。
        // Reactor 线程是可以利用多核，如：机器有 128 核，那么底层会启动 128 线程。
        // 每个线程能都会维持一个 EventLoop。线程之间是无锁的，指令可以被 128 核 CPU 并行执行。
        // 考虑到操作系统调度存在一定程度的性能损失，可以设置为 CPU 核数 * 2，以便最大化利用 CPU 的每一个核。
        // reactor_num 建议设置为 CPU 核数的 1-4 倍
        // reactor_num 必须小于或等于 worker_num 如果设置的 reactor_num 大于 worker_num，会自动调整使 reactor_num 等于 worker_num
        //'reactor_num' => 1,
        // 设置启动的Worker进程数量。Swoole采用固定Worker进程的模式。全异步非阻塞服务器 worker_num配置为CPU核数的1-4倍即可。
        // 如何和php-fpm配合的服务端程序，这个worker_num最好设置成能够和fpm进程数量相匹配的数量，压测的时候在来具体调整吧
        'worker_num' => 32,
        // Listen队列长度,
        //'backlog' => 256,
        // 此选项表示每隔多久轮循一次，单位为秒。如 heartbeat_check_interval => 60，
        // 表示每 60 秒，遍历所有连接，如果该连接在 120 秒内（heartbeat_idle_time 未设置时默认为 interval 的两倍），
        // 没有向服务器发送任何数据，此连接将被强制关闭。若未配置，则不会启用心跳，该配置默认关闭
        'heartbeat_check_interval' => 60,
        // TCP连接的最大闲置时间，单位s , 如果某fd最后一次发包距离现在的时间超过heartbeat_idle_time会把这个连接关闭。
        // 如果只设置了 heartbeat_idle_time 未设置 heartbeat_check_interval 底层将不会创建心跳检测线程
        // PHP 代码中可以调用 heartbeat 方法手工处理超时的连接
        'heartbeat_idle_time ' => 300,
        // 日志文件
        'log_file' => RUNTIME_PATH.'/log/WebsocketSwooleServer.log',
        // 主进程id
        'pid_file' => RUNTIME_PATH.'/WebsocketSwooleServer.pid',
        // 证书cert wss 开启说明，可以只用nginx代理转发实现 wss且可以去掉端口号，也可以单独使用此项功能开启wss但是无法去掉端口号
        //'ssl_cert_file' => RUNTIME_PATH.'/cert/xxx.pem',
        // 证书key
        //'ssl_key_file' => RUNTIME_PATH.'/cert/xxx.pem',
        // 进程用户
        //'user' => 'root',
        // 进程用户组
        //'group' => 'root',
        // 如开启异步安全重启, 需要在workerExit释放连接池资源
        //'reload_async' => true,
        // 设置 Worker 进程收到停止服务通知后最大等待时间【默认值：3】
        //'max_wait_time' => 3,
        // 设置 worker 进程的最大任务数。【默认值：0 即不会退出进程
        // 一个 worker 进程在处理完超过此数值的任务后将自动退出，进程退出后会释放所有内存和资源
        //'max_request' => 1,
        // 数据包分发策略。【默认值：2】
        // 模式值	模式	作用
        // 1 轮循模式收到会轮循分配给每一个 Worker进程
        // 2 固定模式根据连接的文件描述符分配 Worker。这样可以保证同一个连接发来的数据只会被同一个 Worker 处理
        // 3 抢占模式主进程会根据 Worker 的忙闲状态选择投递，只会投递给处于闲置状态的 Worker
        // 4 IP分配根据客户端 IP 进行取模 hash，分配给一个固定的 Worker 进程。
        // 可以保证同一个来源 IP 的连接数据总会被分配到同一个 Worker 进程。算法为 ip2long(ClientIP) % worker_num
        // 5 UID分配需要用户代码中调用 Server->bind() 将一个连接绑定 1 个 uid。然后底层根据 UID 的值分配到不同的 Worker 进程。
        // 算法为 UID % worker_num，如果需要使用字符串作为 UID，可以使用 crc32(UID_STRING)
        // 7 stream模式空闲的Worker会accept连接，并接受Reactor的新请求
        //'dispatch_mode' => 1,
    ],
    // 下面是框架自定义配置
    // 开启/关闭调试模式 true false
    'debug' => true,
    // 是否开启ssl true false 通过nginx转发了，这里不用开启ssl
    'is_ssl' => false,
    // swoole 运行的2种模式 SWOOLE_BASE SWOOLE_PROCESS 区别见官网
    'swoole_mode' => SWOOLE_PROCESS,
    // 接收swoole服务的事件类
    'swoole_event' => '\app\websocket\Event',
];