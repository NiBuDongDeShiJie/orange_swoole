<?php

/**
 * 当前模块的函数库
 * 使用频率高的函数放这里
 * 公共函数库
 * 框架已经自动加载了这个文件
 */

/**
 * websocket数据返回
 * @author zk
 * @param $code
 * @param $msg
 * @param array $data
 * @param string $action 行为标记、区分客户端不同的业务类型
 * @return mixed
 */
function returnMessage($code, $msg, $action = '', $data = [])
{
    $returnData['code'] = intval($code);
    $returnData['msg'] = $msg;
    $returnData['action'] = $action;
    $returnData['data'] = (object)$data;
    return $returnData;
}

/**
 * websocket专用发送数据到客户端
 * @author zk
 * @param $server swoole server类
 * @param $fd 链接id
 * @param string $data 发送数据
 * @return bool
 */
function send($server, $fd, $data = '')
{
    if (empty($fd) || is_null($fd) || $fd <= 0) {
        return false;
    }
    // 是否在线判断
    if (!$server->isEstablished($fd)) {
        return false;
    }
    if (is_array($data)) {
        $data = json_encode($data);
    }
    if (!$server->push($fd, $data)) {
        return false;
    }
    return true;
}

/**
 * 根据token信息获取绑定的fd连接id
 * @param $token 用户token
 * @return int
 */
function getFd($token)
{
    if (!$token) {
        return 0;
    }
    // 从缓存里面取对应的关系
    /*$prefix = CONFIG['redis']['prefix'];
    $connectionKey = $prefix.'connection_'.$token;
    $fd = $this->redis->get($connectionKey);
    if ($fd) {
        return $fd;
    }*/
    return 0;
}

/**
 * 登录判断
 * @param $fd
 * @param $userId
 * @return bool
 */
function checkLogin($fd, $userId)
{
    if ($fd != getFd($userId)) {
        return false;
    }
    return true;
}