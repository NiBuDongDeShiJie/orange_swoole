<?php
namespace app\demo;

use \orangeswoole\event\EventRedisServer;

/**
 * 事件类
 * 可以在这里单独写当前模块的事件
 * 并在事件里面处理自己的业务
 * Class Event
 * @package app\demo
 */
class Event extends EventRedisServer
{
    /**
     * 进程启动初始化工作
     * onWorkerStart
     * @author zk
     * @param $server swoole服务对象
     * @param $workerId 进程id
     * @throws \Exception
     */
    public function onWorkerStart($server, $workerId)
    {
        // todo 进程启动初始化工作
    }
}