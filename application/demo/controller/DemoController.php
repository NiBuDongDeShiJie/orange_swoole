<?php
namespace app\demo\controller;

use app\common\controller\CommonController;
use app\demo\logic\DemoLogic;

/**
 * 注册控制器
 * Class DemoController
 * @package app\demo\controller
 */
class DemoController extends CommonController
{

    /**
     * 这里演示一个访问url的协程并发
     * @param $data
     * @return mixed
     */
    public function foreachCurl($data)
    {
        $logic = new DemoLogic();
        return $logic->foreachCurl($data);
    }

    /**
     * 这里演示一个执行mysql的sql语句的协程并发
     * @param $data
     * @return mixed
     */
    public function foreachSql($data)
    {
        $logic = new DemoLogic();
        return $logic->foreachSql($data);
    }

    /**
     * 逐个并发执行sql，协程go函数的返回值不分先后顺序，看具体的sql在服务器端执行的长短决定耗时
     * @param $data
     * @return mixed
     */
    public function querySql($data)
    {
        $logic = new DemoLogic();
        return $logic->querySql($data);
    }

}