<?php
namespace app\demo\model;

use orangeswoole\model\Model;

/**
 * 测试模型
 * Class TestModel
 * @package app\demo\model
 */
class TestModel extends Model
{

    /**
     * 和tp5一致，这里是定义当前模型所使用的数据库连接
     */
    // public $connect = 'other_db_config';

    /**
     * 和tp5一致，这里是定义当前模型的表
     */
    // public $table = 'app_access_token';
}