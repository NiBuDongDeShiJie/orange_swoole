<?php
namespace app\demo\logic;

use app\common\model\UserModel;
use orangeswoole\logic\Logic;
use tools\ElasticSearchClient;

/**
 * 注册逻辑文件
 * 实际业务的话，可以选择放到swoole里面来写，想要改动小的话，
 * 你可以在之前的业务中生成sql语句，发送到这里来并发执行，达到并发提速的效果
 * Class DemoLogic
 * @package app\demo\logic
 */
class DemoLogic extends Logic
{

    /**
     * 这里演示一个访问url的协程并发
     * 循环go协程批量异步执行sql，最后同步返回结果
     * es服务器端可以采集sql插件就可以轻松的用关系型数据库的sql语法了
     * $data = ['select * from es_1_index where 1=1', 'select * from es_1_index where 1=1'];
     * @param $data
     * @return mixed
     */
    public function foreachCurl($data)
    {
        // $data参数自己验证下

        // 协程组管理工具
        $waitGroup = new \Swoole\Coroutine\WaitGroup();
        $returnData = [];
        // 处理循环体的sql语句
        foreach ($data as $key => $value) {
            // 协程增加组成员
            $waitGroup->add();
            // 创建协程
            go(function () use ($waitGroup, $key, $value, &$returnData) {
                $elasticSearchClient = new ElasticSearchClient();
                $result = $elasticSearchClient->query($value);
                $returnData[$key] = $result;
                // 协程执行完成标记
                $waitGroup->done();
            });
        }
        // 等待所有的协程执行完毕
        $waitGroup->wait();
        return returnData(1000, '操作成功', $returnData);
    }

    /**
     * 这里演示一个执行mysql的sql语句的协程并发
     * 循环go协程批量异步执行sql，最后同步返回结果
     * $data = ['select * from mysql_table1 where 1=1', 'select * from mysql_table2 where 1=1'];
     * @param $data
     * @return mixed
     */
    public function foreachSql($data)
    {
        // $data参数自己验证下

        // 协程组管理工具
        $waitGroup = new \Swoole\Coroutine\WaitGroup();
        $returnData = [];
        // 处理循环体的sql语句
        foreach ($data as $key => $value) {
            // 协程增加组成员
            $waitGroup->add();
            // 创建协程
            go(function () use ($waitGroup, $key, $value, &$returnData) {
                // 关于mysql的异步执行，一个协程里面最好单独实例化一个数据库操作类，mysql的orm可以做连接池，但是我这里mysql并没有
                // 添加连接池，可以自己慢慢来实现，有时间把hyperf的mysql orm（普通的mysql orm不能直接用进来，不支持协程） composer 下来就可以直接使用了。
                $userModel = new UserModel();
                $result = $userModel->query($value);
                if ($result) {
                    $result = reset($result);
                }
                $returnData[$key] = $result;
                // 协程执行完成标记
                $waitGroup->done();
            });
        }
        // 等待所有的协程执行完毕
        $waitGroup->wait();
        return returnData(1000, '操作成功', $returnData);
    }

    /**
     * 逐个并发执行sql，协程go函数的返回值不分先后顺序，看具体的sql在服务器端执行的长短决定耗时
     * @param $data
     * @return mixed
     */
    public function querySql($data)
    {
        // $data参数自己验证下

        // 协程组管理工具
        $waitGroup = new \Swoole\Coroutine\WaitGroup();
        $returnData = [];
        // 处理客户端发送的sql语句
        $sql1 = $data['sql1'];
        $sql2 = $data['sql2'];
        $sql3 = $data['sql3'];
        // 协程增加组成员
        $waitGroup->add();
        // 创建协程
        go(function () use ($waitGroup, $sql1, &$returnData) {
            $elasticSearchClient = new ElasticSearchClient();
            $result = $elasticSearchClient->query($sql1);
            $result = $result['count']['value'];
            $returnData['sql1_count'] = $result;
            // 协程执行完成标记
            $waitGroup->done();
        });

        go(function () use ($waitGroup, $sql2, &$returnData) {
            $elasticSearchClient = new ElasticSearchClient();
            $result = $elasticSearchClient->query($sql2);
            $result = $result['count']['value'];
            $returnData['sql2_count'] = $result;
            // 协程执行完成标记
            $waitGroup->done();
        });

        go(function () use ($waitGroup, $sql3, &$returnData) {
            $elasticSearchClient = new ElasticSearchClient();
            $result = $elasticSearchClient->query($sql3);
            $result = $result['count']['value'];
            $returnData['sql3_count'] = $result;
            // 协程执行完成标记
            $waitGroup->done();
        });

        // 等待所有的协程执行完毕
        $waitGroup->wait();
        return returnData(1000, '操作成功', $returnData);
    }
}
