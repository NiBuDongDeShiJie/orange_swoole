<?php
namespace tools;

/**
 * 公共工具类
 * Class Common
 * @package tools
 */
class Common
{
    /**
     * 参数过滤
     * @param $filter
     * @param string $returnType i（int） f(float) s(string)
     * @return bool|float|int|string|string[]|null
     */
    public static function filter($filter, $returnType = 'i')
    {
        $returnType = strtolower($returnType);
        switch ($returnType) {
            //int
            case 'i' :
                $filter = intval($filter);
                break;
            //float
            case 'f' :
                $filter = floatval($filter);
                break;
            //string
            case 's' :
                //可以增加规则
                $filterArray = [
                    "/<(\\/?)(script|i?frame|style|html|body|title|link|meta|object|\\?|\\%)([^>]*?)>/isU",
                    "/(<[^>]*)on[a-zA-Z]+\s*=([^>]*>)/isU",
                    "/select\b|insert\b|update\b|delete\b|drop\b|;|\"|\'|\/\*|\*|\.\.\/|\.\/|union|into|load_file|outfile|dump/is"
                ];
                $filter = preg_replace($filterArray, '', $filter);
                $filter = strip_tags($filter);
                break;
            default :
                return false;
        }
        return $filter;
    }

    /**
     * 文件日志
     * @param string $content 内容
     * @param string $secondDir 二级目录
     * @param string $baseDir 根目录
     *
     * @return bool
     */
    public static function writeLogs($content = '', $secondDir = '', $baseDir = 'log')
    {
        $baseDir = RUNTIME_PATH . '/' . $baseDir;
        //如果文件不存在
        if (!is_dir($baseDir)) {
            mkdir($baseDir, 0777);
        }
        $baseDir .= '/' . $secondDir;
        //如果文件不存在
        if (!is_dir($baseDir)) {
            mkdir($baseDir, 0777);
        }
        $nowTime = date('Y-m-d');
        $file = $baseDir . '/' . $nowTime  . '.log';
        $contentText = date('Y-m-d H:i:s') . '-';
        $contentText .= $content;
        $contentText .= "\r\n";
        //如果写入失败
        if (file_put_contents($file, $contentText, FILE_APPEND)) {
            return true;
        }
        return false;
    }

    /**
     * 获取文件后缀
     * @param $name 文件
     * @return mixed
     */
    public static function getFileType($name)
    {
        $arr = explode('.', $name);
        $type = end($arr);
        return $type;
    }

    /**
     * 返回汉字首字母
     * @param $str 目标字符串
     * @return string
     */
    public static function getFirstLetter($str)
    {
        //为空
        if (empty($str)) {
            return '';
        }
        $fir = $fchar = ord($str[0]);
        if ($fchar >= ord('A') && $fchar <= ord('z')) {
            return strtoupper($str[0]);
        }
        $s1 = @iconv('UTF-8', 'gb2312', $str);
        $s2 = @iconv('gb2312', 'UTF-8', $s1);
        $s = $s2 == $str ? $s1 : $str;
        if (!isset($s[0]) || !isset($s[1])) {
            return '';
        }
        $asc = ord($s[0]) * 256 + ord($s[1]) - 65536;
        if (is_numeric($str)) {
            return $str;
        }
        //范围判断
        if (($asc >= -20319 && $asc <= -20284) || $fir == 'A') {
            return 'A';
        }
        if (($asc >= -20283 && $asc <= -19776) || $fir == 'B') {
            return 'B';
        }
        if (($asc >= -19775 && $asc <= -19219) || $fir == 'C') {
            return 'C';
        }
        if (($asc >= -19218 && $asc <= -18711) || $fir == 'D') {
            return 'D';
        }
        if (($asc >= -18710 && $asc <= -18527) || $fir == 'E') {
            return 'E';
        }
        if (($asc >= -18526 && $asc <= -18240) || $fir == 'F') {
            return 'F';
        }
        if (($asc >= -18239 && $asc <= -17923) || $fir == 'G') {
            return 'G';
        }
        if (($asc >= -17922 && $asc <= -17418) || $fir == 'H') {
            return 'H';
        }
        if (($asc >= -17417 && $asc <= -16475) || $fir == 'J') {
            return 'J';
        }
        if (($asc >= -16474 && $asc <= -16213) || $fir == 'K') {
            return 'K';
        }
        if (($asc >= -16212 && $asc <= -15641) || $fir == 'L') {
            return 'L';
        }
        if (($asc >= -15640 && $asc <= -15166) || $fir == 'M') {
            return 'M';
        }
        if (($asc >= -15165 && $asc <= -14923) || $fir == 'N') {
            return 'N';
        }
        if (($asc >= -14922 && $asc <= -14915) || $fir == 'O') {
            return 'O';
        }
        if (($asc >= -14914 && $asc <= -14631) || $fir == 'P') {
            return 'P';
        }
        if (($asc >= -14630 && $asc <= -14150) || $fir == 'Q') {
            return 'Q';
        }
        if (($asc >= -14149 && $asc <= -14091) || $fir == 'R') {
            return 'R';
        }
        if (($asc >= -14090 && $asc <= -13319) || $fir == 'S') {
            return 'S';
        }
        if (($asc >= -13318 && $asc <= -12839) || $fir == 'T') {
            return 'T';
        }
        if (($asc >= -12838 && $asc <= -12557) || $fir == 'W') {
            return 'W';
        }
        if (($asc >= -12556 && $asc <= -11848) || $fir == 'X') {
            return 'X';
        }
        if (($asc >= -11847 && $asc <= -11056) || $fir == 'Y') {
            return 'Y';
        }
        if (($asc >= -11055 && $asc <= -10247) || $fir == 'Z') {
            return 'Z';
        }
        return '';
    }

    /**
     * 下载网络图片到本地
     * @param $fileUrl url
     * @param bool $randomFileName 是否随机名字
     *
     * @return mixed
     */
    public static function gatherFileByUrl($fileUrl, $randomFileName = true)
    {
        //参数判断
        if (!$fileUrl) {
            return returnStatus(false, '参数错误');
        }
        $base64File = @file_get_contents($fileUrl);
        //文件读取失败
        if (!$base64File) {
            return returnStatus(false, '源文件错误');
        }
        $path = ROOT_PATH . 'public' .DS . 'static'. DS;
        //文件不存在
        if(!file_exists($path)) {
            mkdir ($path, 0777,true);
        }
        $path .= 'uploads' . DS;
        //路径不存在
        if(!file_exists($path)) {
            mkdir ($path, 0777,true);
        }
        $path .= date('Y-m-d') . DS;
        //路径不存在
        if(!file_exists($path)) {
            mkdir ($path, 0777,true);
        }
        $pathInfo = @pathinfo($fileUrl);
        $suffix = self::getFileType($fileUrl);
        //如果随机名字
        if ($randomFileName) {
            $fileName = date('YmdHis') . mt_rand(1000 , mt_rand(1001, 9999));
        } else {
            $fileName = $pathInfo['filename'];
        }
        $fileName .=  '.' . $suffix;
        $savePath = $path . $fileName;
        //保存失败
        if (!@file_put_contents($savePath, $base64File)) {
            return returnStatus(false, '保存失败');
        }
        $dbFilePath = '/static/uploads/' . date('Y-m-d') . '/' . $fileName;
        return returnStatus(true, '保存成功', $dbFilePath);
    }

    /**
     * 二维数组排序
     * @param $array 目标数组
     * @param $sort_key 排序key
     * @param int $sort_order SORT_ASC
     * @return bool
     */
    public static function arraySort($array, $sort_key, $sort_order = SORT_ASC)
    {
        //是数据的情况
        if(is_array($array)){
            //遍历
            foreach ($array as $row_array){
                $key_array[] = $row_array[$sort_key];
            }
            array_multisort($key_array, $sort_order, $array);
            return $array;
        }
        return false;
    }

    /**
     * 下划线字符串转换成驼峰命名
     * @param $str 目标字符串
     * @param bool $ucFirst 首字母是否小写
     *
     * @return mixed|string
     */
    public static function convertUnderline($str , $ucFirst = true)
    {
        $str = ucwords(str_replace('_', ' ', $str));
        $str = str_replace(' ','',lcfirst($str));
        return $ucFirst ? ucfirst($str) : $str;
    }

    /**
     * 产生随机字符串，不长于32位
     * @param int $length 长度
     *
     * @return string
     */
    public static function getRandomString($length = 32)
    {
        $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
        $str = "";
        //循环
        for ($i = 0; $i < $length; $i++ ) {
            $str .= substr($chars, mt_rand(0, strlen($chars)-1), 1);
        }
        return $str;
    }

    /**
     * 模拟提交参数，支持https提交 可用于各类api请求
     * @author zk
     * @param $url 提交的地址
     * @param string $data POST数组
     * @param string $method POST/GET，默认GET方式
     * @param string $referer
     * @param string $cookie
     * @return bool|string
     */
    public static function curlHttp($url, $data='', $method='GET', $referer = '', $cookie='')
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 对认证证书来源的检查
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); // 从证书中检查SSL加密算法是否存在
        curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
        curl_setopt($ch, CURLOPT_ENCODING, "gzip");
        if ($method=='POST') {
            curl_setopt($ch, CURLOPT_POST, 1); // 发送一个常规的Post请求
            if ($data != '') {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data); // Post提交的数据包
            }
        }
        curl_setopt($ch, CURLOPT_TIMEOUT, 60); // 设置超时限制防止死循环
        curl_setopt($ch, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
        curl_setopt($ch, CURLOPT_COOKIE, $cookie); // 获取的信息以文件流的形式返回
        curl_setopt($ch, CURLOPT_REFERER, $referer);
        $tmpInfo = curl_exec($ch); // 执行操作
        curl_close($ch); // 关闭CURL会话
        return $tmpInfo; // 返回数据
    }
}
