<?php
namespace tools;

/**
 * es客户端封装
 * 如果用第三方的es客户端，内存容易泄露，自己封装一个简单的查询客户端代码少，泄露风险小
 * Class ElasticSearchClient
 * @package tools
 */
class ElasticSearchClient
{

    /**
     * ElasticSearchClient constructor.
     */
    public function __construct()
    {

    }

    /**
     * 这个查询方法可以根据自己的业务稍微改进下
     * 封装一个执行sql语句的方法
     * @param $sql
     * @return array
     */
    public function query($sql)
    {
        $result = $this->curlGet($sql);
        $resultSet = [];
        if (!empty($result['aggregations'])) {
            foreach ($result['aggregations'] as $k=>$item) {
                if (strstr($k , '@NESTED')) {
                    $k = str_replace('@NESTED' , '' , $k);
                    $nestedParm = explode('.' , $k);
                    if (isset($item[$k]['buckets'])) {
                        $resultSet[$nestedParm[1]] = $item[$k]['buckets'];
                    }
                } elseif($k == 'count') { // 统计count的时候 as 一个固定的key tp_count 这样才能取到数据
                    $resultSet[$k] = $item;
                } else {
                    $resultSet[$k] = $item['buckets'];
                }
            }
        } else { //非group形式的返回
            $resultSet = $result;
        }
        return $resultSet;
    }

    /**
     * 原生curl客户端
     * @param $sql
     * @param $post
     * @return bool|mixed|string
     */
    private function curlGet($sql, $post = '')
    {
        $config = config('elasticSearch', 'elastic_search');
        $sql = urlencode($sql);
        $url = "{$config['protocol']}://{$config['host']}:{$config['port']}/_sql/?sql={$sql}";
        $curl = curl_init();
        $auth = "{$config['user']}:{$config['password']}";// 用户名:密码
        $headers[] = "Content-Type:application/json";
        $post = json_encode($post);
        curl_setopt($curl, CURLOPT_URL, $url); // 设置url
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_USERPWD, $auth); // 设置用户名密码
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers); // 设置请求头
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post); //设置请求体
        curl_setopt($curl, CURLOPT_TIMEOUT,300);   //只需要设置一个秒的数量就可以
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 要求结果为字符串且输出到屏幕上
        $result = curl_exec($curl);
        if ($result == false) {
            $error = curl_error($curl);
            return $error;
        }
        curl_close($curl);
        $result = swoole_substr_json_decode($result, 0, -1, true);
        return $result;
    }
}
