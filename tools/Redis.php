<?php
namespace tools;

/**
 * redis启动类
 * Class Common
 * @package tools
 */
class Redis
{
    /**
     * @var redis实例
     */
    public $redis;

    /**
     * Redis constructor.
     */
    public function __construct()
    {
        $host = CONFIG['redis']['host'];
        $port = CONFIG['redis']['port'];
        $password = CONFIG['redis']['password'];
        $dbNumber = CONFIG['redis']['db_number'];
        // 启动php原生 redis类
        $redis = new \Redis();
        $result = $redis->connect($host, $port);
        // 密码验证
        if ($password) {
            $redis->auth($password);
        }
        // 数据库选择
        if ($dbNumber) {
            $redis->select($dbNumber);
        }
        if (!$result) {
            // todo 抛出异常
        }
        $this->redis = $redis;
    }
}
